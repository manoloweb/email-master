# Email Master
Follow instructions to install and setup your copy, all details are below this section. Once done, you should be able to use it as follows:

### How to Use
1. Run the Application:
   - Launch the application by running `python main.py` in your terminal. (See 'Usage' section for more details)
   - The interface will prompt you to select the email tone and paste the original message.

2. Generate Email:
   - Click the "Generate Email" button to utilize the OpenAI API and receive suggestions and proposed content based on your input.

3. Review and Copy:
   - The application will display suggestions and a proposed email message.
   - Review the suggestions and proposed message in the provided sections.
   - You can copy the content and use it to compose your final email.

### Benefits

- **Efficiency:** The Email Generator streamlines the process of crafting emails, providing suggestions and structured content that aligns with different tones and styles.
- **Consistency:** The application helps maintain consistent email communication by offering predefined templates and content for various situations.
- **Time Saving:** Generate emails quickly without spending excessive time on wording and structure.
- **Enhanced Communication:** Effectively communicate your message with well-organized and impactful email content.
- **Adaptability:** Easily adjust the tone of the email based on your recipient and the context of the message.

Try out the Email Generator and experience the convenience of composing professional and well-structured emails with ease!

-----

## Install
Simply clone the repository into a local folder, then install the dependencies by doing:

    %pip install -r requirements.txt

## Getting started
Change your personal data in the first section on the file `prompts.py`

## Environment variables
`OPENAI_API_KEY`

Copy the file `.env.template` to a file named `.env` in the root folder of the project (the same folder this readme is in), and edit `.env` adding you own **OpenAI API Key**

See below for instructions on how to get your own OpenAI API key

**Generating an OpenAI API Key**

To use the Email Generator application, you'll need an API key from OpenAI. Here's how you can obtain one:

**Sign in or Sign up:**

- If you already have an OpenAI account, sign in. If not, you can sign up for an account on the OpenAI website.

- Navigate to API Section: Once you're logged in, navigate to the API section of your OpenAI account dashboard.

**Create a New API Key:**

- Click on the "Create API Key" button or a similar option, depending on the layout of the dashboard.
- Give your API key a recognizable name, such as "Email Generator App."
If applicable, set any desired usage restrictions or permissions for the API key.

**Get the API Key:**

- After creating the API key, you'll be presented with a screen displaying the generated API key.
- Copy the API key to your clipboard. This key will be used to authenticate requests to the OpenAI API in the Email Generator application.

**Add API Key to Configuration:**

- In your local copy of the Email Generator application's code, locate the section where the OpenAI API key is configured.
- Replace the placeholder for the API key with the key you copied from the OpenAI dashboard.

**Run the Application:**

- With the API key properly configured, you can now run the Email Generator application and use the OpenAI API to generate effective emails.

_Please note that API keys are sensitive information and should be treated as credentials. Keep your API key secure and do not expose it publicly._

## Usage
To run, simply navigate to your folder using terminal (or VS Code, and run a terminal within) and run this command:

    %python main.py

or, in some cases:

    %python3 main.py

Important note: In the resulting interface, spaces appear to be removed, but once you copy the text, they are there :) 