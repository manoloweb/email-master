import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QTextEdit, QPushButton, QComboBox, QDialog, QPlainTextEdit
from dotenv import load_dotenv
import os
from prompts import persona, expected_output, prompts, general_prompt, main_config
import openai
import re

load_dotenv()
openai_key = os.getenv("OPENAI_API_KEY")

def improve_text(text):
    # Add proper space after periods and commas using regular expressions
    text = re.sub(r'\.(?=\w)', r'. ', text)
    text = re.sub(r',(?=\w)', r', ', text)
    return text
    


class EmailApp(QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        self.tone_combo = QComboBox()
        self.tone_combo.addItems(["Formal", "Familiar", "Informal"])

        self.message_input = QTextEdit()
        self.generate_button = QPushButton("Generate Email")

        layout.addWidget(QLabel("Select Tone:"))
        layout.addWidget(self.tone_combo)
        layout.addWidget(QLabel("Paste Original Message:"))
        layout.addWidget(self.message_input)
        layout.addWidget(self.generate_button)

        self.generate_button.clicked.connect(self.generate_email)

        self.setLayout(layout)
        self.setWindowTitle("Email Generator")

    def generate_email(self):
        openai.api_key = openai_key
        selected_tone = self.tone_combo.currentText().lower()
        original_message = self.message_input.toPlainText()
        # Call OpenAI API here and get the generated email
        message_tone = prompts[selected_tone]
        prompt = general_prompt.format(name=main_config["name"], age=main_config["age"], gender=main_config["gender"], company=main_config["company"], position=main_config["position"], personality=main_config["personality"], tone_name=selected_tone, tone_description=message_tone, original_message=original_message)
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages = [
                {"role": "system", "content": persona},
                {"role": "system", "content": expected_output},
                {"role": "user", "content": prompt}
            ]
        )
        generated_response = response.choices[0].message.content
        response_parts = generated_response.split("-----")
        if len(response_parts) >= 2:
            suggestions = response_parts[0]
            proposed_message = response_parts[1]
        else :
            suggestions = "There was an error generating suggestions"
            proposed_message = "There was an error generating the email: " + generated_response

        # Create a modal dialog to show the generated email
        dialog = QDialog(self)
        dialog.setWindowTitle("Generated Email")

        layout = QVBoxLayout()

        suggestions = improve_text(suggestions)
        proposed_message = improve_text(proposed_message)

        explanation_label = QLabel("Suggested Changes and Clarifications:")
        email_changes = QPlainTextEdit()  
        email_changes.setPlainText(suggestions)

        generated_label = QLabel("Generated Email:")
        generated_email = QPlainTextEdit()  
        generated_email.setPlainText(proposed_message)

        layout.addWidget(explanation_label)
        layout.addWidget(email_changes)
        layout.addWidget(generated_label)
        layout.addWidget(generated_email)

        dialog.setLayout(layout)
        dialog.exec_()
        # Clear the input fields and reset combo box after the dialog is closed
        self.message_input.clear()
        self.tone_combo.setCurrentIndex(0)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = EmailApp()
    window.show()
    sys.exit(app.exec_())
